package com.rocket.service;


import static java.util.Collections.singletonList;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.rocket.data.Node;

@RunWith(MockitoJUnitRunner.class)
public class DirectoryServiceTest {

	private DirectoryService target;

	@Before
	public void setUp() {
		target = new DirectoryService();
	}

	@Test
	public void testCountFolders_OneFolder() {
		Integer result = target.calculate(singletonList(getNode("folder")), "folder");
		assert 1 == result;
	}

	@Test
	public void testCountFolders_FolderwithFolderChildren() {
		Node folderNode = getNode("folder");
		folderNode.setChildren(singletonList(getNode("folder")));
		assert 2 == target.calculate(singletonList(folderNode), "folder");
	}

	@Test
	public void testCountFolders_testEmpty() {
		Node node = new Node();
		assert 0 == target.calculate(singletonList(node), "folder");
	}

	@Test
	public void testCountFolders_testOneFile() {
		Node file = getNode("file");
		assert 0 == target.calculate(singletonList(file), "folder");

	}

	@Test
	public void testCountFolders_testFileInsideFolder() {
		Node file = getNode("file");
		Node folder = getNode("folder");
		folder.setChildren(singletonList(file));
		assert 1 == target.calculate(singletonList(folder), "folder");
	}

	@Test
	public void testCountFiles_oneFile() {
		Node file = getNode("file");
		assert 1 == target.calculate(singletonList(file), "file");
	}

	@Test
	public void testCountFiles_multipleFolders_withMultipleFiles() {
		Node file = getNode("file");
		Node file2 = getNode("file");
		Node file3 = getNode("file");
		Node file4 = getNode("file");

		Node folder = getNode("folder");
		Node folder2 = getNode("folder");
		Node folder3 = getNode("folder");
		Node folder4 = getNode("folder");

		folder.setChildren(Arrays.asList(file, folder2));
		folder2.setChildren(Arrays.asList(file2, file3, file4));
		folder3.setChildren(singletonList(folder2));
		folder4.setChildren(singletonList(folder3));

		assert 4 == target.calculate(singletonList(folder), "file");
	}

	@Test
	public void testCountFolders_multipleFolders_withMultipleFiles() {
		Node file = getNode("file");
		Node file2 = getNode("file");
		Node file3 = getNode("file");
		Node file4 = getNode("file");

		Node folder = getNode("folder");
		Node folder2 = getNode("folder");
		Node folder3 = getNode("folder");
		Node folder4 = getNode("folder");

		folder.setChildren(Arrays.asList(file, folder4));
		folder2.setChildren(Arrays.asList(file2, file3, file4));
		folder3.setChildren(singletonList(folder2));
		folder4.setChildren(singletonList(folder3));

		assert 4 == target.calculate(singletonList(folder), "folder");
	}

	@Test
	public void testFileSize() {
		Node node = getNode("file");
		node.setSize("200");
		assert 200 == target.calculate(singletonList(node), "size");
	}

	@Test
	public void testFilesSize() {
		Node file = getNodeWithSize("file", "200");
		Node file2 = getNodeWithSize("file", "200");
		Node file3 = getNodeWithSize("file", "200");
		Node file4 = getNodeWithSize("file", "200");

		Node folder = getNode("folder");
		Node folder2 = getNode("folder");
		Node folder3 = getNode("folder");
		Node folder4 = getNode("folder");

		folder.setChildren(Arrays.asList(file, folder4));
		folder2.setChildren(Arrays.asList(file2, file3, file4));
		folder3.setChildren(singletonList(folder2));
		folder4.setChildren(singletonList(folder3));
		assert 800 == target.calculate(singletonList(folder), "size");
	}

	private Node getNode(String type) {
		Node node = new Node();
		node.setType(type);
		return node;
	}

	private Node getNodeWithSize(String type, String size) {
		Node node = new Node();
		node.setType(type);
		node.setSize(size);
		return node;
	}
}
