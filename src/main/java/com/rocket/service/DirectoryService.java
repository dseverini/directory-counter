package com.rocket.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.rocket.data.Node;

@Service
public class DirectoryService {
	public Integer calculate(List<Node> directory, String type) {
		int count = 0;
		for (Node node: directory) {
			count += getNodeCount(node, type);
			if (node.getChildren() != null && !node.getChildren().isEmpty()) {
				count += calculate(node.getChildren(), type);
			}
		}
		return count;
	}

	public int getNodeCount(Node node, String type) {
		if (node.getType() != null) {
			if (type.equals("size")) {
				if (node.getType().equals("file")) {
					return Integer.valueOf(node.getSize());
				}
			}
			if (type.equals(type) && node.getType().equals(type)){
				return 1;
			}
		}
		return 0;
	}
}