package com.rocket.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DirectoryResponse {

	private Integer folders;
	private Integer files;
	private Integer size;

}
