package com.rocket.data;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Node {

	private String type;
	private String name;
	private String size;
	private List<Node> children;
}
