package com.rocket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rocket.data.DirectoryResponse;
import com.rocket.data.Node;
import com.rocket.service.DirectoryService;

@RestController
@RequestMapping("/directory")
class DirectoryController {

	private DirectoryService directoryService;

	@Autowired
	public DirectoryController(DirectoryService directoryService) {
		this.directoryService = directoryService;
	}

	@PostMapping("/count-folders")
	public ResponseEntity<DirectoryResponse> countFolders(@RequestBody List<Node> request) {
		DirectoryResponse body = new DirectoryResponse();
		body.setFolders(directoryService.calculate(request, "folder"));
		return ResponseEntity.ok(body);
	}

	@PostMapping("/count-files")
	public ResponseEntity<DirectoryResponse> countFiles(@RequestBody List<Node> request) {
		DirectoryResponse body = new DirectoryResponse();
		body.setFiles(directoryService.calculate(request, "file"));
		return ResponseEntity.ok(body);
	}

	@PostMapping("/files-size")
	public ResponseEntity<DirectoryResponse> calculateSize(@RequestBody List<Node> request) {
		DirectoryResponse body = new DirectoryResponse();
		body.setSize(directoryService.calculate(request, "size"));
		return ResponseEntity.ok(body);
	}

}
